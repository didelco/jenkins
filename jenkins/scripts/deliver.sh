#!/usr/bin/env sh

echo 'The following "npm" command builds your Node.js/React application for production'
set -x
npm run build
set +x
echo 'The following "npm" command runs your Node.js/React application in'
echo 'development mode and makes the application available for web browsing.'
set -x
npm start &
sleep 1
echo $! > .pidfile
set +x
echo 'Visit http://localhost:3000 to see your Node.js/React application in action.'